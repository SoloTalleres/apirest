/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package restapi;

/**
 *
 * @author oscarmendezaguirre
 */


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class RestApi {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UserService userService = new UserService();
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            String userInfo = userService.getUserInfo();
            JsonNode json = objectMapper.readTree(userInfo);
            String title = json.get("title").asText();
            System.out.println(userInfo);
            System.out.println(title);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
